function renderStudents(T) {
    sudentsNbr = T.length;
    if (sudentsNbr > 0) {
        var render = '<table class="table table-striped">';
        render += "<tr><th>Id</th><th>First Name</th><th>Last Name</th><th>Email</th><th>CIN</th><th>Absent</th><th>Présent</th><th>Suprimer</th><th>Editer</th><th>Afficher</th></tr>";
        for (i = 0; i < sudentsNbr; i++) {
            var student = T[i];
            render += "<tr><td>" + student.id + "</td><td>" + student.fname + "</td><td>" + student.lname + "</td>" +
                "</td><td>" + student.email + "</td>" + "</td><td>" + student.cin + "</td>" +
                `</td><td> <button class='btn btn-danger'  id='btnAbscent${student.id}' onclick='T(${student.id})' > Absent </button></td>
                 <td> <button class='btn btn-success' id='btnPresent${student.id}' onclick='P(${student.id})' > Présent </button></td>
                 <td> <button class='btn btn-primary' id='btnDelete${student.id}' onclick='DeleteUser(${student.id})' > Suprimer </button></td>
                 <td> <button class='btn btn-primary' id='btnedit${student.id}' onclick='Edituser(${student.id})' > Editer </button></td>
                 <td> <a href="displayStudent.html"> <button class='btn btn-success' id='btn' onclick='S(${student.id})' > Afficher </button></a></td></tr>`
        }
        render += "</table>";
        var newTable = document.getElementById("myTable");
        newTable.innerHTML = render;
    } else {
        document.getElementById("noStudent").style.display = "block";
    }
}

function loadStudents() {
    return localStorage.getItem("students") || "[]";
}

function T(x) {
    abscents = JSON.parse(localStorage.getItem("abscents") || "[]");
    abscent = searchById(x);
    abscents.push(abscent);
    localStorage.setItem("abscents", JSON.stringify(abscents));
    // Make button disabled
    btnAbscent = document.getElementById(`btnAbscent${x}`);
    btnAbscent.disabled = true;
}

function P(x) {
    presents = JSON.parse(localStorage.getItem("presents") || "[]");
    present = searchById(x);
    presents.push(present);
    localStorage.setItem("presents", JSON.stringify(presents));
    // Make button disabled
    btnPresent = document.getElementById(`btnPresent${x}`);
    btnPresent.disabled = true;
}

function S(x) {

    UniqueStudents = searchById(x);
    localStorage.setItem("UniqueStudents", JSON.stringify(UniqueStudents));

}

function renderUnique(T) {

    var render = '<table class="table table-striped">';
    render += "<tr><th>Id</th><th>First Name</th><th>Last Name</th><th>Email</th><th>CIN</th></tr>";
    var UniqueStudents = JSON.parse(localStorage.getItem("UniqueStudents"));
    render += "<tr><td>" + UniqueStudents.id + "</td><td>" + UniqueStudents.fname + "</td><td>" + UniqueStudents.lname + "</td>" +
        "</td><td>" + UniqueStudents.email + "</td>" + "</td><td>" + UniqueStudents.cin + "</td></tr>";

    render += "</table>";
    var newTable = document.getElementById("myTable");
    newTable.innerHTML = render;
}




function searchById(x) {
    students = JSON.parse(localStorage.getItem("students"));
    for (let i = 0; i < students.length; i++) {
        if (students[i].id == Number(x)) {
            res = students[i];
        }
    }
    console.log(`searched student by ${x}`, res);
    return res;
}

function searchByCIN() {
    val = document.getElementById("search").value;
    students = JSON.parse(loadStudents());
    res = [];
    for (let i = 0; i < students.length; i++) {
        if (students[i].cin == val) {
            res.push(students[i]);
        }
    }
    renderStudents(res);
}

function DeleteUser(id) {
    students = JSON.parse(loadStudents());
    students.splice(searchIndex(id, students), 1);
    localStorage.setItem("students", JSON.stringify(students));
    console.log(students);
    location.reload();

}

function searchIndex(id, tab) {
    for (let i = 0; i < tab.length; i++) {
        if (tab[i].id == id) {
            res = i;
        }
    }
}

function Edituser(id) {
    student = searchById(id);
    var editForm = document.getElementById("editForm");
    editForm.innerHTML = `<div class="container"
<h1 class="centre> Modifier vos infos </h1>
<div class="form-group" >
<label>Nom</label>
<input type="text" id="fname" class="form-control" id="fname" value="${student.fname}"
<label>prénom</label>
<input type="text" id="lname" class="form-control" id="lname" value="${student.lname}"
<label>Nom</label>
<input type="text" id="email" class="form-control" id="email" value="${student.email}"
<label>Nom</label>
<input type="text" id="cin" class="form-control" id="cin" value="${student.cin}"
</div>
<button class='btn btn-primary' id='addBtn' onclick='validateEdit(${id})' > Editer </button>`

}

function validateEdit(id) {
    // Get modified values from edit form
    fname = document.getElementById("fname").value;
    lname = document.getElementById("lname").value;
    email = document.getElementById("email").value;
    cin = document.getElementById("cin").value;
    //get all students from local storage
    student = JSON.parse(loadStudents());
    student = {
        id: id,
        fname: fname,
        lname: lname,
        email: email,
        cin: cin
    }
    students.splice(searchIndex(id, students), 1);
    students.splice(searchIndex(id, students), 0, student);
    localStorage.setItem("students", JSON.stringify(students));
    renderStudents(students);
    editForm.style.display = "none";
    location.reload();
}

function displayStudent(id) {

}