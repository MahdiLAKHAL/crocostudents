//this function allows to register user 
function addUser() {
    fName = document.getElementById("Fname").value;
    lName = document.getElementById("Lname").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    ConPassword = document.getElementById("ConPassword").value;
    country = document.getElementById("country").value;
    UserType = document.getElementById("UserType").value;
    tel = document.getElementById("tel").value;
    console.log(fName, lName, email, password, ConPassword, country, UserType, tel);
    Users = JSON.parse(localStorage.getItem("Users") || "[]");
    max = localStorage.getItem("max") || "1";
    user = {
        id: Number(max),
        fname: fName,
        lname: lName,
        email: email,
        password: password,
        ConPassword: ConPassword,
        country: country,
        tel: tel,
        UserType: UserType
    }
    console.log("users", user);
    if (Users.length > 0) {

        if (verifuniqEmail(email, Users)) {
            Users.push(user);
            localStorage.setItem("Users", JSON.stringify(Users));
            localStorage.setItem("max", Number(max) + 1);
        }
    } else {
        Users.push(user);
        localStorage.setItem("Users", JSON.stringify(Users));

    }

    function verifuniqEmail(email, Users) {
        res = false;
        for (let i = 0; i < Users.length; i++) {
            if (email != Users[i].email) {
                res = true;
            }
        }
        return res;
    }
}


function signin() {
    email = document.getElementById("loginmail").value;
    pwd = document.getElementById("loginpwd").value;

    users = JSON.parse(localStorage.getItem("Users") || "[]");

    while (counter <= 3) {


        for (let i = 0; i < Users.length; i++) {

            if (email == Users[i].email && password == Users[i].password) {
                UserRedirection(Users[i].Usertype);
            } else {
                document.getElementById("errorMsg").innerHTML = "Email or Password is invalid!!"
            }
        }
    }
}

function UserRedirection(x) {
    if (x == "admin") {
        location.replace("dashboard.html")
    } else {
        location.replace("calculateurdemoyen.html")
    }
}